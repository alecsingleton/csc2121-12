package brass;

import java.util.List;

class BrassUpgradePortState implements BrassComputerPlayerState
{
	private BrassGame brass_game;
	private BrassAIComputerPlayer computer_player;
	
	public BrassUpgradePortState(BrassGame bg, BrassAIComputerPlayer cp)
	{
		brass_game = bg;
		computer_player = cp;
	}
	
	public BrassComputerPlayerAction getBrassMove()
	{
		System.out.println("IN UPGRADE PORTS STATE!!!!!!!!");
		BrassComputerPlayerAction computer_move = new BrassComputerPlayerAction();
		int computer_player_id = brass_game.getActivePlayerID();
		
		int card_index = getCardForNonBuildAction(computer_player_id);
		
		computer_move.selectTechUpgradeAction(card_index, BrassIndustryEnum.PORT.getValue(), BrassIndustryEnum.PORT.getValue());
		computer_player.changeState(computer_player.getBuyPortState());
		return computer_move;
	}

	private int getCardForNonBuildAction(int player_id)
	{
		int num_cards = brass_game.getNumCards(player_id);
		boolean brass_phase = brass_game.getBrassPhase();
		
		//first iteration of the list of cards.
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id < 20)
			{
				int city_id = brass_card_city_tech_id;
				if (brass_game.isCityFull(city_id)) return i;
				
				//duplicate city cards or already built in city (and canal phase)
				if (!brass_phase)
				{
					int num_tokens_in_city = brass_game.getNumTokensInCity(city_id, player_id);
					if (num_tokens_in_city > 0) return i;
					
					for (int j = i+1; j <= num_cards; j++)
					{
						int city_id_duplicate = brass_game.getCardCityTechID(j);
						if (city_id == city_id_duplicate) return i;
					}
				}
			}
		
			if (brass_card_city_tech_id == 24) return i; //shipyard industry card
			if (brass_card_city_tech_id == 1)  return i; //barrow
			if (brass_card_city_tech_id == 2) return i;  //birkenhead
			
			//duplicate industry cards
			if (brass_card_city_tech_id > 19 && brass_card_city_tech_id != 23)
			{
				int industry_id = brass_card_city_tech_id;// - 19;
				for (int j = i+1; j <= num_cards; j++)
				{
					int industry_id_duplicate = brass_game.getCardCityTechID(j);
					if (industry_id == industry_id_duplicate) return i;
				}
			}
		}
		
		//second pass through the cards
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id == 6) return i;  //bury
			if (brass_card_city_tech_id == 19) return i;  //wigan
			
			if (brass_card_city_tech_id == 22) return i;  //iron works
			if (brass_card_city_tech_id == 20) return i;  //coal
			
			if (brass_card_city_tech_id == 14) return i;  //oldham
			if (brass_card_city_tech_id == 5) return i;  //burnley
		}
		 
		 //pick a random card to use for the non build action
		util.Random rand = util.Random.getRandomNumberGenerator();
		int random_discard = rand.randomInt(1, num_cards);
		while (!brass_game.canSelectCard(random_discard, player_id))
		{
			random_discard = rand.randomInt(1, num_cards);
		}
	//System.out.println("random_discard: " + random_discard);
		return random_discard;
	}
}