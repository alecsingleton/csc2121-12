package brass;

import java.util.List;

class BrassAIComputerPlayer implements BrassComputerPlayer
{
	private BrassGame brass_game;
	private BrassComputerPlayerState current_state;
	private BrassComputerPlayerState upgrade_state;
	private BrassComputerPlayerState buy_port_state;
	private BrassComputerPlayerState sell_cotton_state;
	private BrassComputerPlayerState end_canal_state;
	private BrassComputerPlayerState buy_coal_state;
	
	public BrassAIComputerPlayer(BrassGame bg)
	{
		brass_game = bg;
		upgrade_state = new BrassUpgradePortState(bg, this);
		buy_port_state = new BrassBuyPortsState(bg, this);
		sell_cotton_state = new BrassSellCottonState(bg, this);
		end_canal_state = new BrassEndCanalState(bg, this);
		buy_coal_state = new BrassBuyCoalState(bg, this);
		current_state = upgrade_state;
	}
	
	public BrassComputerPlayerState getBuyPortState(){
		return buy_port_state;
	}
	
	public BrassComputerPlayerState getBuyCoalState(){
		return buy_coal_state;
	}
	
	public BrassComputerPlayerState getEndCanalState(){
		return end_canal_state;
	}
	
	public BrassComputerPlayerState getSellCottonState(){
		return sell_cotton_state;
	}
	
	public void changeState(BrassComputerPlayerState state){
		current_state = state;
	}
	
	public BrassComputerPlayerAction getBrassMove()
	{
		BrassComputerPlayerAction action = current_state.getBrassMove();
		return action;
	}
}