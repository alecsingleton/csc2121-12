package brass;

import java.util.List;

class BrassSellCoalAndIronComputerPlayer implements BrassComputerPlayer
{
	private BrassGame brass_game;
	private int TurnCounter;
	private boolean HasSoldCotton;
	private boolean BuiltACotton;
	private boolean LoanHasBeenTaken;
	private boolean CoalUpgraded;
	private int ShipyardUpgrade;
	private int LinkCounter;
	
	public BrassSellCoalAndIronComputerPlayer(BrassGame bg)
	{
		brass_game = bg;
		BuiltACotton = false;
		LoanHasBeenTaken = false;
		HasSoldCotton = false;
		CoalUpgraded = false;
		ShipyardUpgrade = 0;
	}
	
	public BrassComputerPlayerAction getBrassMove()
	{
		BrassComputerPlayerAction computer_move = new BrassComputerPlayerAction();
		int computer_player_id = brass_game.getActivePlayerID();

		// Avoiding magic numbers! (kind of)
		int CoalIndustryValue = 20;
		int CottonIndustryValue = 21;
		int IronIndustryValue = 22;
		int PortIndustryValue = 23;
		int ShipyardIndustryValue = 24;

		int LoanThreshold = 12;

		if (brass_game.isFirstTurn())
		{

			int CottonCardToUse = GetIndustryOrCityCard(computer_player_id, CottonIndustryValue);
			if (CottonCardToUse > 0)
			{
	
				if(CheckPortCitiesWithCottonCard(computer_move, CottonCardToUse))
				{
					BuiltACotton = true;
					return computer_move;
				}

				else if (CheckPortCitiesWithCitiesCards(computer_move))
				{
					BuiltACotton = true;
					return computer_move;
				}


				else if (CheckExternalCitiesWithCottonCard(computer_move, CottonCardToUse))
				{
					BuiltACotton = true;
					return computer_move;
				}
			}

	
			else if (CheckPortCitiesWithCitiesCards(computer_move))
			{
				BuiltACotton = true;
				return computer_move;
			}


			else if(CheckExternalCitiesWithCitiesCards(computer_move))
			{
				BuiltACotton = true;
				return computer_move;
			}

	
			getAllInfoForIndustrySpecificBuildAction(computer_move, computer_player_id, BrassIndustryEnum.COTTON.getValue());
			if (computer_move.isActionSelected()) 
			{
				BuiltACotton = true;
				return computer_move;
			}

		} 
		

		if(BuiltACotton && !HasSoldCotton)
		{
			boolean can_sell_cotton = brass_game.canSellCotton(computer_player_id);
			can_sell_cotton = brass_game.canSellCotton(computer_player_id);

			if(can_sell_cotton && !LoanHasBeenTaken)
			{
				LoanHasBeenTaken = true;
				computer_move.selectTakeLoanAction(getCardForNonBuildAction(computer_player_id), 3);
				return computer_move;
			}

			else if(can_sell_cotton && LoanHasBeenTaken)
			{
				HasSoldCotton = true;
				computer_move.selectSellCottonAction(getCardForNonBuildAction(computer_player_id));
				return computer_move;
			}


			else 
			{
				getAllInfoForLinkToSellCottonAction(computer_move, computer_player_id, 1);
				if (computer_move.isActionSelected()) 
				{
					return computer_move;
				}
			}
		}

		if (HasSoldCotton)
		{
			if (!CoalUpgraded)
			{
				CoalUpgraded = true;
				computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 1, 3);
				return computer_move;
			}

	
			if(brass_game.countAllPlayersUnflippedIndustry(CoalIndustryValue - 19, computer_player_id) > 0)
			{
				if(brass_game.countAllPlayersUnflippedIndustry(IronIndustryValue - 19, computer_player_id) > 0)
				{/*
					if(ShipyardUpgrade < 3)
					{
						ShipyardUpgrade = ShipyardUpgrade + 2;
						computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 5, 5);
						return computer_move;
					}

					if(ShipyardUpgrade == 3 && brass_game.getBrassPhase())
					{
						ShipyardUpgrade++;
						computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 5, 2);
						return computer_move;
					}*/
				}

				else if(brass_game.getBrassPhase() && LinkCounter < 6)
				{
			
					getAllInfoForSortedLinkAction(computer_move, computer_player_id);
					if(computer_move.isActionSelected()) 
					{ 
						LinkCounter++;
						return computer_move;
					}
				}
			}


			getAllInfoForIndustrySpecificBuildAction(computer_move, computer_player_id, ShipyardIndustryValue - 19);
			if(computer_move.isActionSelected())
			{
				//ShipyardUpgrade++;
				return computer_move;
			}


			getAllInfoForIndustrySpecificBuildAction(computer_move, computer_player_id, IronIndustryValue - 19);
			if(computer_move.isActionSelected())
			{
				return computer_move;
			}

			getAllInfoForIndustrySpecificBuildAction(computer_move, computer_player_id, CoalIndustryValue - 19);
			if(computer_move.isActionSelected())
			{
				return computer_move;
			}
		}


		getAllInfoForSortedLinkAction(computer_move, computer_player_id);
		if(computer_move.isActionSelected()) 
		{ 
			return computer_move;
		}

		int player_money = brass_game.getMoney(computer_player_id);
		if(player_money < LoanThreshold)
		{
			computer_move.selectTakeLoanAction(getCardForNonBuildAction(computer_player_id), 3);
			return computer_move;
		}

		if(brass_game.isTokenStackEmpty(computer_player_id, CottonIndustryValue - 19))
		{
			computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 2, 2);
			return computer_move;
		}

		if(brass_game.isTokenStackEmpty(computer_player_id, PortIndustryValue - 19))
		{
			computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 4, 4);
			return computer_move;
		}

		computer_move.selectDiscardAction(getCardForNonBuildAction(computer_player_id));
		return computer_move;
	}



	private int GetIndustryOrCityCard(int player_id, int industry_or_city_id)
	{
		int num_cards = brass_game.getNumCards(player_id);
		int brass_card_city_tech_id;

		for (int i = 1; i <= num_cards; i++)
		{
			brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			if (brass_card_city_tech_id == industry_or_city_id)
			{
				return i;
			}
		}

		return -1;
	}

	private boolean CheckPortCitiesWithCottonCard(BrassComputerPlayerAction computer_move, int card_to_use)
	{

		int LancasterCityID = 10;
		int PrestonCityID = 15;
		int WarringtonCityID = 18;
		int CottonIndustryValue = 21;
		int computer_player_id = brass_game.getActivePlayerID();

		if(brass_game.isCityConnectedToConstructedPort(PrestonCityID) && brass_game.canBuildIndustry(false, PrestonCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, PrestonCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else if(brass_game.isCityConnectedToConstructedPort(LancasterCityID) && brass_game.canBuildIndustry(false, LancasterCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, LancasterCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else if(brass_game.isCityConnectedToConstructedPort(WarringtonCityID) && brass_game.canBuildIndustry(false, WarringtonCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, WarringtonCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else
		{
			return false;
		}
	}

	private boolean CheckPortCitiesWithCitiesCards(BrassComputerPlayerAction computer_move)
	{
	
		int LancasterCityID = 10;
		int PrestonCityID = 15;
		int WarringtonCityID = 18;
		int CottonIndustryValue = 21;
		int computer_player_id = brass_game.getActivePlayerID();

		int LancasterHandID = GetIndustryOrCityCard(computer_player_id, LancasterCityID);
		int PrestonHandID = GetIndustryOrCityCard(computer_player_id, PrestonCityID);
		int WarringtonHandID = GetIndustryOrCityCard(computer_player_id, WarringtonCityID);

		if(LancasterHandID >= 0 && brass_game.isCityConnectedToConstructedPort(PrestonCityID) && brass_game.canBuildIndustry(true, PrestonCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(LancasterHandID, PrestonCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else if(PrestonHandID >= 0 && brass_game.isCityConnectedToConstructedPort(LancasterCityID) && brass_game.canBuildIndustry(true, LancasterCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(PrestonHandID, LancasterCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else if(WarringtonHandID >= 0 && brass_game.isCityConnectedToConstructedPort(WarringtonCityID) && brass_game.canBuildIndustry(true, WarringtonCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(WarringtonHandID, WarringtonCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else
		{
			return false;
		}
	}

	private boolean CheckExternalCitiesWithCottonCard(BrassComputerPlayerAction computer_move, int card_to_use)
	{
		
		int ColneCityID = 7;
		int MacclesfieldCityID = 12;
		int RochdaleCityID = 16;
		int CottonIndustryValue = 21;
		int computer_player_id = brass_game.getActivePlayerID();

		for (int i = 1; i < 5; i++) 
		{
			if(i == computer_player_id)
			{
				break;
			} 

			else if(brass_game.getNumTokensInCity(ColneCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(false, ColneCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(card_to_use, ColneCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}

			else if(brass_game.getNumTokensInCity(MacclesfieldCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(false, MacclesfieldCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(card_to_use, MacclesfieldCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}

			else if(brass_game.getNumTokensInCity(RochdaleCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(false, RochdaleCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(card_to_use, RochdaleCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}
		}

	
		if (brass_game.canBuildIndustry(false, ColneCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, ColneCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		if (brass_game.canBuildIndustry(false, RochdaleCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, RochdaleCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		if (brass_game.canBuildIndustry(false, MacclesfieldCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, MacclesfieldCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		return false;
	}

	private boolean CheckExternalCitiesWithCitiesCards(BrassComputerPlayerAction computer_move)
	{
		int ColneCityID = 7;
		int MacclesfieldCityID = 12;
		int RochdaleCityID = 16;
		int CottonIndustryValue = 21;
		int computer_player_id = brass_game.getActivePlayerID();

		int ColneHandID = GetIndustryOrCityCard(computer_player_id, ColneCityID);
		int MacclesfieldHandID = GetIndustryOrCityCard(computer_player_id, MacclesfieldCityID);
		int RochdaleHandID = GetIndustryOrCityCard(computer_player_id, RochdaleCityID);

		for (int i = 1; i < 5; i++) 
		{
			if (ColneHandID < 0 && MacclesfieldHandID < 0  && RochdaleHandID < 0 )
			{
				break;
			}

			if(i == computer_player_id)
			{
				break;
			} 

			else if(ColneHandID >= 0 && brass_game.getNumTokensInCity(ColneCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(true, ColneCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(ColneHandID, ColneCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}

			else if(MacclesfieldHandID >= 0 && brass_game.getNumTokensInCity(MacclesfieldCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(true, MacclesfieldCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(MacclesfieldHandID, MacclesfieldCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}

			else if(RochdaleHandID >= 0 && brass_game.getNumTokensInCity(RochdaleCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(true, RochdaleCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(RochdaleHandID, RochdaleCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}
		}
		return false;
	}

	private boolean CheckShipyardBuilding(BrassComputerPlayerAction computer_move, int player_id)
	{
		int BarrowCityID = 1;
		int BirkenheadCityID = 2;
		int LiverpoolCityID = 11;
		int ShipyardIndustryValue = 24;
		int LiverpoolHandID = GetIndustryOrCityCard(player_id, LiverpoolCityID);
		int ShipyardCard = GetIndustryOrCityCard(player_id, ShipyardIndustryValue);

		if(brass_game.canBuildIndustry(false, LiverpoolCityID, ShipyardIndustryValue - 19, player_id))
		{
			computer_move.selectBuildAction(ShipyardCard, LiverpoolCityID, ShipyardIndustryValue - 19, 0, 0);
			return true;
		}

		else if(brass_game.canBuildIndustry(true, LiverpoolCityID, ShipyardIndustryValue - 19, player_id))
		{
			computer_move.selectBuildAction(LiverpoolHandID, LiverpoolCityID, ShipyardIndustryValue - 19, 0, 0);
			return true;
		}

		return false;
	}

	// ******************************* Functions that had already existed

	private int getCardForNonBuildAction(int player_id)
	{
		int num_cards = brass_game.getNumCards(player_id);
		boolean brass_phase = brass_game.getBrassPhase();
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id < 20)
			{
				int city_id = brass_card_city_tech_id;
				if (brass_game.isCityFull(city_id)) return i;
				
	
				if (!brass_phase)
				{
					int num_tokens_in_city = brass_game.getNumTokensInCity(city_id, player_id);
					if (num_tokens_in_city > 0) return i;
					
					for (int j = i+1; j <= num_cards; j++)
					{
						int city_id_duplicate = brass_game.getCardCityTechID(j);
						if (city_id == city_id_duplicate) return i;
					}
				}
			}

			if (brass_card_city_tech_id == 21) return i; 
			if (brass_card_city_tech_id == 23) return i; 
			if (brass_card_city_tech_id == 7)  return i;
			if (brass_card_city_tech_id == 8) return i;   
			if (brass_card_city_tech_id == 10) return i;  
			if (brass_card_city_tech_id == 12) return i; 
			
	
			if (brass_card_city_tech_id > 19)
			{
				int industry_id = brass_card_city_tech_id;// - 19;
				for (int j = i+1; j <= num_cards; j++)
				{
					int industry_id_duplicate = brass_game.getCardCityTechID(j);
					if (industry_id == industry_id_duplicate && industry_id != 20) return i; // Avoid disgarding coal;
				}
			}
		}
		
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id == 24) return i;
			if (brass_card_city_tech_id == 1)  return i; 
			if (brass_card_city_tech_id == 2) return i; 
		}
		 

		util.Random rand = util.Random.getRandomNumberGenerator();
		int random_discard = rand.randomInt(1, num_cards);
		while (!brass_game.canSelectCard(random_discard, player_id))
		{
			random_discard = rand.randomInt(1, num_cards);
		}

		return random_discard;
	}
	
	private void getAllInfoForIndustrySpecificBuildAction(BrassComputerPlayerAction computer_move, int player_id, int industry_id)
	{

		int num_cards = brass_game.getNumCards(player_id);
		int city_id;
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
				
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			if (brass_card_city_tech_id <= 19)
			{
				city_id = brass_card_city_tech_id;
				if (brass_game.canBuildIndustry(true, city_id, industry_id, player_id))
				{
					int coal_city_id = brass_game.canMoveCoal(city_id, industry_id, player_id);
					int iron_city_id = brass_game.canMoveIron(industry_id, player_id);
					if (coal_city_id >= 0 && iron_city_id >= 0) 
					{
						computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
						return;
					}
				}
			}
			else
			{
				int card_industry_id = brass_card_city_tech_id - 19;
				if (industry_id == card_industry_id)
				{

					for (int j = 1; j <= 19; j++)
					{
						city_id = j;
						if (brass_game.canBuildIndustry(false, city_id, industry_id, player_id))
						{
							int coal_city_id = brass_game.canMoveCoal(city_id, industry_id, player_id);
							int iron_city_id = brass_game.canMoveIron(industry_id, player_id);
							if (coal_city_id >= 0 && iron_city_id >= 0) 
							{
								computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
								return;
							}
						}
					}
				}
			}
		}
	}
	

	private void getAllInfoForLinkToSellCottonAction(BrassComputerPlayerAction computer_move, int computer_player_id, int max_depth_limit)
	{
		if (max_depth_limit < 1) max_depth_limit = 1;
		int num_connections = brass_game.getNumLinks();

		int curr_depth_limit = 1;
		int curr_depth = 1;
		
		while(!computer_move.isActionSelected() && curr_depth_limit <= max_depth_limit)
		{

			for (int i = 1; i <= num_connections; i++)
			{

				if (computer_move.isActionSelected()) return;
				
				boolean can_build_link = brass_game.canBuildLink(i, computer_player_id);
				if (can_build_link)
				{
					brass_game.buildTestLink(i, computer_player_id);
					boolean can_player_sell_cotton = brass_game.canSellCotton(computer_player_id);
					if (can_player_sell_cotton)
					{
						computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), i);
	System.out.println("Sell cotton link top level");					
					}
					else if (curr_depth < curr_depth_limit)
					{
						getAllInfoForLinkToSellCottonActionRec(computer_move, computer_player_id, curr_depth_limit, curr_depth + 1, num_connections, i);
					}
				brass_game.removeTestLink(i);
				}
			}
			curr_depth_limit++;
		}
	}
	
	private void getAllInfoForLinkToSellCottonActionRec(BrassComputerPlayerAction computer_move, int computer_player_id, int max_depth, int curr_depth, int num_connections, int first_connection)
	{
		for (int i = 1; i <= num_connections; i++)
		{
		
			if (computer_move.isActionSelected()) return;
			
			boolean can_build_link = brass_game.canBuildLink(i, computer_player_id);
			if (can_build_link)
			{
				brass_game.buildTestLink(i, computer_player_id);
				boolean can_player_sell_cotton = brass_game.canSellCotton(computer_player_id);
				if (can_player_sell_cotton)
				{
					computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), first_connection);
	System.out.println("Sell cotton link depth = " + curr_depth);					
				}
				else if (curr_depth < max_depth)
				{
					getAllInfoForLinkToSellCottonActionRec(computer_move, computer_player_id, max_depth, curr_depth + 1, num_connections, first_connection);
				}
				brass_game.removeTestLink(i);
			}
		}
	}
	
	private void getAllInfoForSortedLinkAction(BrassComputerPlayerAction computer_move, int computer_player_id)
	{	
		List<Integer> sorted_links = brass_game.getSortedLinks();
		int num_connections = sorted_links.size();
		for (int i = 1; i <= num_connections; i++)
		{
			int link_id = sorted_links.get(i-1);
			boolean can_build_link = brass_game.canBuildLink(link_id, computer_player_id);
			if (can_build_link)
			{
				computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), link_id);
			
				return;
			}
		}
	}
	
	private void getAllInfoForLinkAction(BrassComputerPlayerAction computer_move, int computer_player_id)
	{
		int num_connections = brass_game.getNumLinks();
		for (int i = 1; i <= num_connections; i++)
		{
			boolean can_build_link = brass_game.canBuildLink(i, computer_player_id);
			if (can_build_link)
			{
				computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), i);
				
				return;
			}
		}
	}
}
