package brass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
  * Created by las on 04-13-17.
 */
 public class alec_mattComputerPlayer implements BrassComputerPlayer {
    private BrassGame brass_game;
    private int player_id;
	private int link_count;
	private boolean built_cotton;
	private boolean Has_s_Cotton;
	private boolean LHBT;
	
	public alec_mattComputerPlayer(BrassGame bg) {
      built_cotton= false;
	  brass_game = bg;
	  Has_s_Cotton=false;
	  LHBT=false;
    }
 	public BrassComputerPlayerAction getBrassMove()
	{
		BrassComputerPlayerAction computer_move = new BrassComputerPlayerAction();
		player_id= brass_game.getActivePlayerID();
		//int num_actions_already_taken = brass_game.getNumActionsTaken(player_id);
		boolean can_s_Cotton = brass_game.canSellCotton(player_id);
		int loanNeed =10;
		int player_money = brass_game.getMoney(player_id);
	
		int num_computer_players_unflipped_ports = brass_game.countAllPlayersUnflippedIndustry(BrassIndustryEnum.PORT.getValue(), player_id);
		if (num_computer_players_unflipped_ports == 0)
		{
			getAllInfoForIndustrySpecificBuildAction(computer_move, player_id, BrassIndustryEnum.PORT.getValue());
			if (computer_move.isActionSelected()) return computer_move;
		}
		

		int num_computer_players_unflipped_cotton_mills = brass_game.countAllPlayersUnflippedIndustry(BrassIndustryEnum.COTTON.getValue(), player_id);
		if (num_computer_players_unflipped_cotton_mills == 0)
		{
			getAllInfoForIndustrySpecificBuildAction(computer_move, player_id, BrassIndustryEnum.COTTON.getValue());
			if (computer_move.isActionSelected()) return computer_move;
		}
		

		if (!can_s_Cotton)
		{
			getAllInfoForLinkToSellCottonAction(computer_move, player_id, 1);
			if (computer_move.isActionSelected()) return computer_move;
		}
		
		getAllInfoForSortedLinkAction(computer_move, player_id);
		if (computer_move.isActionSelected()) return computer_move;
		
	

		
		
		
		//-------------------------------
		if(built_cotton && !Has_s_Cotton)
		{
			can_s_Cotton = brass_game.canSellCotton(player_id);

			if(can_s_Cotton && LHBT)
			{
				Has_s_Cotton = true;
				computer_move.selectSellCottonAction(carddbuiltaction(player_id));
				return computer_move;
			}

	
			else 
			{
				getAllInfoForLinkToSellCottonAction(computer_move, player_id, 1);
				if (computer_move.isActionSelected()) 
				{
					return computer_move;
				}
			}
		}
		if (Has_s_Cotton)
		{
			
			if(brass_game.countAllPlayersUnflippedIndustry(20 - 19, player_id) > 0)
			{
			if(brass_game.getBrassPhase() && link_count < 6)
				{

					getAllInfoForSortedLinkAction(computer_move, player_id);
					if(computer_move.isActionSelected()) 
					{ 
						link_count++;
						return computer_move;
					}
				}
			}
			getAllInfoForIndustrySpecificBuildAction(computer_move, player_id, 24 - 19);
			if(computer_move.isActionSelected())
			{
				//ShipyardUpgrade++;
				return computer_move;
			}
			getAllInfoForIndustrySpecificBuildAction(computer_move, player_id, 22 - 19);
			if(computer_move.isActionSelected())
			{
				return computer_move;
			}

			getAllInfoForIndustrySpecificBuildAction(computer_move, player_id, 20 - 19);
			if(computer_move.isActionSelected())
			{
				return computer_move;
			}
		}

		getAllInfoForSortedLinkAction(computer_move, player_id);
		if(computer_move.isActionSelected()) 
		{ 
			return computer_move;
		}


		if(brass_game.isTokenStackEmpty(player_id, 21 - 19))
		{
			computer_move.selectTechUpgradeAction(carddbuiltaction(player_id), 2, 2);
			return computer_move;
		}

		if(brass_game.isTokenStackEmpty(player_id, 23 - 19))
		{
			computer_move.selectTechUpgradeAction(carddbuiltaction(player_id), 4, 4);
			return computer_move;
		}
	

		int num_computer_players_unflipped_ports2 = brass_game.countAllPlayersUnflippedIndustry(BrassIndustryEnum.PORT.getValue(), player_id);
		if (num_computer_players_unflipped_ports2 == 0)
		{
			getAllInfoForIndustrySpecificBuildAction(computer_move, player_id, BrassIndustryEnum.PORT.getValue());
			if (computer_move.isActionSelected()) return computer_move;
		}
		
	
		int num_computer_players_unflipped_cotton_mills2 = brass_game.countAllPlayersUnflippedIndustry(BrassIndustryEnum.COTTON.getValue(), player_id);
		if (num_computer_players_unflipped_cotton_mills2 == 0)
		{
			getAllInfoForIndustrySpecificBuildAction(computer_move, player_id, BrassIndustryEnum.COTTON.getValue());
			if (computer_move.isActionSelected()) return computer_move;
		}
		if (!can_s_Cotton)
		{
			getAllInfoForLinkToSellCottonAction(computer_move, player_id, 1);
			if (computer_move.isActionSelected()) return computer_move;
		}
		
		getAllInfoForSortedLinkAction(computer_move, player_id);
		if (computer_move.isActionSelected()) return computer_move;

		int player_money1 = brass_game.getMoney(player_id);
		int player_income3 = brass_game.getIncome(player_id);
		if (player_money1 < 7 )//&& player_income3 <= 10)
		{
			computer_move.selectTakeLoanAction(carddbuiltaction(player_id), 3);
			return computer_move;
		}
		
		System.out.println("removed a card");
		computer_move.selectDiscardAction(carddbuiltaction(player_id));
		return computer_move;

	}

	private int carddbuiltaction(int player_id)
	{
		int num_cards = brass_game.getNumCards(player_id);
		boolean brass_phase = brass_game.getBrassPhase();
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id < 20)
			{
				int city_id = brass_card_city_tech_id;
				if (brass_game.isCityFull(city_id)) return i;
				

				if (!brass_phase)
				{
					int num_tokens_in_city = brass_game.getNumTokensInCity(city_id, player_id);
					if (num_tokens_in_city > 0) return i;
					
					for (int j = i+1; j <= num_cards; j++)
					{
						int city_id_duplicate = brass_game.getCardCityTechID(j);
						if (city_id == city_id_duplicate) return i;
					}
				}
			}
		
			if (brass_card_city_tech_id == 24) return i; 
			if (brass_card_city_tech_id == 1)  return i;
			if (brass_card_city_tech_id == 2) return i;

			if (brass_card_city_tech_id > 19)
			{
				int industry_id = brass_card_city_tech_id;
				for (int j = i+1; j <= num_cards; j++)
				{
					int industry_id_duplicate = brass_game.getCardCityTechID(j);
					if (industry_id == industry_id_duplicate) return i;
				}
			}
		}
		

		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id == 6) return i; 
			if (brass_card_city_tech_id == 19) return i;
			
			if (brass_card_city_tech_id == 22) return i;  
			if (brass_card_city_tech_id == 20) return i; 
			
			if (brass_card_city_tech_id == 14) return i; 
			if (brass_card_city_tech_id == 5) return i; 
		}
		 

		util.Random rand = util.Random.getRandomNumberGenerator();
		int random_discard = rand.randomInt(1, num_cards);
		while (!brass_game.canSelectCard(random_discard, player_id))
		{
			random_discard = rand.randomInt(1, num_cards);
		}

		return random_discard;
	}
	
	private void getAllInfoForIndustrySpecificBuildAction(BrassComputerPlayerAction computer_move, int player_id, int industry_id)
	{

		int num_cards = brass_game.getNumCards(player_id);
		int city_id;
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
				
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			if (brass_card_city_tech_id <= 19)
			{
				city_id = brass_card_city_tech_id;
				if (brass_game.canBuildIndustry(true, city_id, industry_id, player_id))
				{
					int coal_city_id = brass_game.canMoveCoal(city_id, industry_id, player_id);
					int iron_city_id = brass_game.canMoveIron(industry_id, player_id);
					if (coal_city_id >= 0 && iron_city_id >= 0) 
					{
						computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
						return;
					}
				}
			}
			else
			{
				int card_industry_id = brass_card_city_tech_id - 19;
				if (industry_id == card_industry_id)
				{
	
					for (int j = 1; j <= 19; j++)
					{
						city_id = j;
						if (brass_game.canBuildIndustry(false, city_id, industry_id, player_id))
						{
							int coal_city_id = brass_game.canMoveCoal(city_id, industry_id, player_id);
							int iron_city_id = brass_game.canMoveIron(industry_id, player_id);
							if (coal_city_id >= 0 && iron_city_id >= 0) 
							{
								computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
								return;
							}
						}
					}
				}
			}
		}
	}
	

	private void getAllInfoForLinkToSellCottonAction(BrassComputerPlayerAction computer_move, int player_id, int max_depth_limit)
	{
		if (max_depth_limit < 1) max_depth_limit = 1;
		int num_connections = brass_game.getNumLinks();

		int curr_depth_limit = 1;
		int curr_depth = 1;
		
		while(!computer_move.isActionSelected() && curr_depth_limit <= max_depth_limit)
		{
	
	
			for (int i = 1; i <= num_connections; i++)
			{

				if (computer_move.isActionSelected()) return;
				
				boolean can_build_link = brass_game.canBuildLink(i, player_id);
				if (can_build_link)
				{
			
					brass_game.buildTestLink(i, player_id);
					boolean can_player_sell_cotton = brass_game.canSellCotton(player_id);
					if (can_player_sell_cotton)
					{
						computer_move.selectLinkAction(carddbuiltaction(player_id), i);
				
					}
					else if (curr_depth < curr_depth_limit)
					{
						getAllInfoForLinkToSellCottonActionRec(computer_move, player_id, curr_depth_limit, curr_depth + 1, num_connections, i);
					}

					brass_game.removeTestLink(i);
				}
			}
			curr_depth_limit++;
		}
	}
	
	private void getAllInfoForLinkToSellCottonActionRec(BrassComputerPlayerAction computer_move, int player_id, int max_depth, int curr_depth, int num_connections, int first_connection)
	{
		for (int i = 1; i <= num_connections; i++)
		{
		
			if (computer_move.isActionSelected()) return;
			
			boolean can_build_link = brass_game.canBuildLink(i, player_id);
			if (can_build_link)
			{
			
				brass_game.buildTestLink(i, player_id);
				boolean can_player_sell_cotton = brass_game.canSellCotton(player_id);
				if (can_player_sell_cotton)
				{
					computer_move.selectLinkAction(carddbuiltaction(player_id), first_connection);
				
				}
				else if (curr_depth < max_depth)
				{
					getAllInfoForLinkToSellCottonActionRec(computer_move, player_id, max_depth, curr_depth + 1, num_connections, first_connection);
				}

				brass_game.removeTestLink(i);
			}
		}
	}
	
	private void getAllInfoForSortedLinkAction(BrassComputerPlayerAction computer_move, int player_id)
	{	
		List<Integer> sorted_links = brass_game.getSortedLinks();
		int num_connections = sorted_links.size();
		for (int i = 1; i <= num_connections; i++)
		{
			int link_id = sorted_links.get(i-1);
			boolean can_build_link = brass_game.canBuildLink(link_id, player_id);
			if (can_build_link)
			{
				computer_move.selectLinkAction(carddbuiltaction(player_id), link_id);
			
				return;
			}
		}
	}
	
	private void getAllInfoForLinkAction(BrassComputerPlayerAction computer_move, int player_id)
	{
		int num_connections = brass_game.getNumLinks();
		for (int i = 1; i <= num_connections; i++)
		{
			boolean can_build_link = brass_game.canBuildLink(i, player_id);
			if (can_build_link)
			{
				computer_move.selectLinkAction(carddbuiltaction(player_id), i);
			
				return;
			}
		}
	}
	
 
	}