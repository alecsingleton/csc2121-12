package brass;

import java.util.List;
import java.util.Iterator;
import java.util.Set;

import util.Random;

public class BrassWeightComputerPlayer implements BrassComputerPlayer
{
    private BrassGame brass_game;
    private BrassComputerActionWeights base_weights;
    private Random rng;

    public BrassWeightComputerPlayer(BrassGame bg)
	{
	    brass_game = bg;
	    base_weights = null;
	    rng = Random.getRandomNumberGenerator();
	}
    
    public BrassComputerPlayerAction getBrassMove()
	{
	    int computer_player_id = brass_game.getActivePlayerID();

	    BrassComputerPlayerAction computer_action = new BrassComputerPlayerAction();

	    int num_actions_already_taken = brass_game.getNumActionsTaken(computer_player_id);

	    BrassComputerActionWeights weights = new BrassComputerActionWeights();
	    //get a list of everything we can do
	    //only things within weights can be done and are assumed to have been vetted as being able to do.

	    System.out.println("Weighing cards");
	    weighCards(weights);

	    System.out.println("Weighing connections");
	    weighConnections(weights);

	    System.out.println("Weighing loans");
            //taking loans
	    if (brass_game.canTakeLoan(computer_player_id,3))
	    {
		int income_weight = base_weights.get("income")*brass_game.getIncome(computer_player_id);
		int money_weight = (int)((double)(base_weights.get("money"))/(double)(1+brass_game.getMoney(computer_player_id)));

		int loan_weight = money_weight - income_weight;
		int loan_amount = 1;
		if (loan_weight >= base_weights.get("loan-threshold-30"))
		    loan_amount = 3;
		else if (loan_weight >= base_weights.get("loan-threshold-20"))
		    loan_amount = 2;
		
		weights.add("loan-"+loan_amount,loan_weight);
	    }

	    System.out.println("Weighing upgrades");
	    //tech upgrading
	    for (int j=1; j<=5; j++)
	    {
		if (brass_game.canTechUpgrade(computer_player_id,j))
		{
		    weights.add("upgrade-"+j,base_weights.get("upgrade-"+j) - (brass_game.countAllPlayersUnflippedIndustry(j,computer_player_id)*base_weights.get("unflipped")));
		}
	    }
	    
	    System.out.println("Weighing selling cotton");
	    //sell cotton
	    if (brass_game.canSellCotton(computer_player_id))
	    {
		weights.add("sell_cotton",base_weights.get("sell_cotton")+(base_weights.get("money_cotton")/(brass_game.getMoney(computer_player_id)+1)));
	    }

	    System.out.println("Done Weighing");

	    if (weights.size() == 0)
	    {
		int card_index = 1;
		//can't do anything, discard
		for (int i =1; i<=brass_game.getNumCards(computer_player_id); i++)
		{
		    if (brass_game.canSelectCard(i,computer_player_id))
		    {
			card_index = i;
			if (rng.randomInt(0,1) == 0) break;
		    }
		}

		computer_action.selectDiscardAction(card_index);
		return computer_action;
	    }

	    Set<String> keys = weights.keySet();
	    Iterator<String> iter = keys.iterator();
	    String best_key = iter.next();
	    int best_weight = weights.get(best_key);
	    
	    while (iter.hasNext())
	    {
		String curr_key = iter.next();
		int curr_weight = weights.get(curr_key);

		System.out.println("Operation: " + curr_key + ", weight: " + curr_weight);

		if (curr_weight > best_weight)
		{
		    best_weight = curr_weight;
		    best_key = curr_key;
		}
		else if (curr_weight == best_weight && rng.randomInt(0,1) == 0)
		{
		    best_weight = curr_weight;
		    best_key = curr_key;
		}
	    }

	    String[] operation = best_key.split("-");
	    System.out.println("Selected action: " + best_key);

	    if (operation[0].equals("build"))
	    {

		int industry_id = Integer.parseInt(operation[1]);
		int card_id = Integer.parseInt(operation[3]);
		int city_id = Integer.parseInt(operation[5]);

		computer_action.selectBuildAction(card_id,city_id,industry_id,brass_game.canMoveCoal(city_id,industry_id,computer_player_id),brass_game.canMoveIron(industry_id,computer_player_id));
		return computer_action;
	    }

	    //need to get a card to discard
	    iter = keys.iterator();
	    String worst_key = iter.next();
	    int worst_weight = weights.get(worst_key);
	    
	    while (iter.hasNext())
	    {
		String curr_key = iter.next();
		int curr_weight = weights.get(curr_key);

		//short out if we already marked a card for discard
		if (curr_key.startsWith("discard"))
		{
		    worst_key = curr_key;
		    break;
		}

		//unfortunetly it is possible that we discard a card which has the highest value to us but also happens to have one of the lowest weight	
		if (curr_key.startsWith("build") && (curr_weight < worst_weight || !worst_key.startsWith("build")))
		{
		    worst_key = curr_key;
		    worst_weight = curr_weight;
		}
	    }
	    
	    String[] card_string = worst_key.split("-");
	    int card_index =0;
	    if (card_string[0].equals("build")) card_index = Integer.parseInt(card_string[3]);
	    else if (card_string[0].equals("discard")) card_index = Integer.parseInt(card_string[2]); //marked for discard
	    else
	    {
		for (int i =1; i<=brass_game.getNumCards(computer_player_id); i++)
		{
		    if (brass_game.canSelectCard(i,computer_player_id))
		    {
			card_index = i;
			if (rng.randomInt(0,3) == 0) break;
		    }
		}		      
	    }
	    
	    System.out.println("Not building, card to discard: " + card_index);
	    

	    if (operation[0].equals("link"))
	    {
		computer_action.selectLinkAction(card_index,Integer.parseInt(operation[1]));
		return computer_action;
	    }
	    	    
	    else if (operation[0].equals("sell_cotton"))
	    {
		computer_action.selectSellCottonAction(card_index);
		return computer_action;
	    }
	    
	    else if (operation[0].equals("loan"))
	    {
		computer_action.selectTakeLoanAction(card_index,Integer.parseInt(operation[1]));
		return computer_action;
	    }
		
	    else if (operation[0].equals("upgrade"))
	    {
		//now we gotta find the second best upgrade
		keys = weights.keySet();
		iter = keys.iterator();
		String second_best_key = iter.next();
		int second_best_weight = weights.get(best_key);

		while (iter.hasNext())
		{
		    String curr_key = iter.next();
		    int curr_weight = weights.get(curr_key);
		    if (curr_key.startsWith("upgrade") && !best_key.equals(curr_key) && (curr_weight > second_best_weight || !second_best_key.startsWith("upgrade")))
		    {
			second_best_key = curr_key;
			second_best_weight = curr_weight;
		    }
		}

		if (!second_best_key.startsWith("upgrade")) //didn't find a second best (couldn't perform another one)
		    second_best_key = best_key;
		
		String[] second_best = second_best_key.split("-");
		computer_action.selectTechUpgradeAction(card_index,Integer.parseInt(operation[1]),Integer.parseInt(second_best[1]));
		return computer_action;
	    }

	    computer_action.selectDiscardAction(card_index);
	    return computer_action;
	}

    public void readWeightsFromFile(String filename)
	{
	    base_weights = new BrassComputerActionWeights();
	    base_weights.readFile(filename);
	}
    public void setBaseWeights(BrassComputerActionWeights weights)
	{
	    base_weights = weights;
	}
    
    private int getWeightForMovingCoal(int city_id, int industry_id)
	{
	    int computer_player_id = brass_game.getActivePlayerID();
	    int move_coal = brass_game.canMoveCoal(city_id,industry_id,computer_player_id);

	    if (move_coal == 0) return 0;

	    else if (move_coal == 20) return base_weights.get("coal_demand");

	    else if (move_coal > 0)
	    {
		if (brass_game.isCityConnectedToPlayerNetwork(move_coal,computer_player_id))
		    return base_weights.get("move_coal_from_network"); //want to be able to give bias to our own coal mines
		return base_weights.get("move_coal_from_other");
	    }
	    else return -1;
	}
    
    private int getWeightForMovingIron(int industry_id)
	{
	    int computer_player_id = brass_game.getActivePlayerID();
	    int move_iron = brass_game.canMoveIron(industry_id,computer_player_id);

	    if (move_iron == 0) return 0;

	    else if (move_iron == 20) return base_weights.get("iron_demand");

	    else if (move_iron > 0)
	    {
		if (brass_game.isCityConnectedToPlayerNetwork(move_iron,computer_player_id))
		    return base_weights.get("move_iron_from_network"); //want to be able to give bias to our own iron mines
		return base_weights.get("move_iron_from_other");
	    }
	    else return -1;
	}

    private int getWeightsForMoving(int city_id, int industry_id)
	{
	    //returns an average
	    int iron = getWeightForMovingIron(industry_id);
	    int coal = getWeightForMovingCoal(city_id,industry_id);

	    if (iron == 0 && coal > 0)
		return coal;
	    if (iron > 0 && coal == 0)
		return iron;
	    return (int)(((double)iron+(double)coal)/2.0);
	}

    
    private void weighConnections(BrassComputerActionWeights weights)
	{
	    int computer_player_id = brass_game.getActivePlayerID();
	    int action_num = brass_game.getNumActionsTaken(computer_player_id);

	    //give weights to canals
	    List<Integer> sortedLinks = brass_game.getSortedLinks();
	    Iterator<Integer> iter = sortedLinks.iterator();

	    //this is so we can weigh the connections differently during the different phases
	    String linkType = "rail";
	    if (!brass_game.getBrassPhase()) linkType = "canal";
	    
	    int count = 0;
	    while (iter.hasNext())
	    {
		Integer link_id = iter.next();

		if (brass_game.getBrassPhase() && action_num == 1)
		{
		    if (brass_game.canBuildExpensiveLink(link_id,computer_player_id))
		    {
			weights.add("link-" + link_id,base_weights.get("link-rail" + "-" + link_id)-count++);
		    }
		    
		}
		else
		{
		    if (brass_game.canBuildLink(link_id,computer_player_id))
		    {
			weights.add("link-" + link_id,base_weights.get("link-" + linkType + "-" + link_id)-count++);
		    }
		}
	    }	    

	}
    
    private void weighCards(BrassComputerActionWeights weights)
	{
	    
	    int computer_player_id = brass_game.getActivePlayerID();
	    //for all cards in hand get a list of all possible operations on all cities with all types of industry

	    //weigh each one
	    //gotta have relative weights based upon what's left on the board
	    //there are 66 cards total with 24 different tech ids
	    //loop over all the cards in our hand, get their industries
	    for (int i =1; i<=brass_game.getNumCards(computer_player_id); i++)
	    {
		if (brass_game.canSelectCard(i,computer_player_id))
		{
		    int operations = weights.size();
		    
		    int tech_id = brass_game.getCardCityTechID(i);
		    int weight_for_card = base_weights.get("tech_id-" + tech_id);
		    if (tech_id <= 19) //city
		    {
			if (brass_game.isCityFull(tech_id))
			{
			    //no point in checking what this card could potentially do as it is better off being discarded for use with another operation
			    continue;
			}
			//build cotton, coal, or iron
			//see what is available
			//see how connected it is
			if (brass_game.isCityConnectedToPlayerNetwork(tech_id,computer_player_id))
			    weight_for_card += base_weights.get("is_connected_to_network");

			for (int j=1; j<=5; j++)
			{
			    if (brass_game.canBuildIndustry(true,tech_id,j,computer_player_id))
			    {
				weights.add("build-" + j + "-card-" + i + "-on-" + tech_id,weight_for_card + base_weights.get("build-" + j) + getWeightsForMoving(tech_id,j) );
			    }
			}
		    }
		    else
		    {
			for (int j =1; j<=19; j++)
			{
			    if (brass_game.isCityFull(j)) continue;

			    int temp_weight = weight_for_card;
			    
			    if (brass_game.isCityConnectedToPlayerNetwork(j,computer_player_id))
				temp_weight += base_weights.get("is_connected_to_network");
			    
			    if (brass_game.canBuildIndustry(false,j,tech_id-19,computer_player_id))
			    {
				weights.add("build-" + (tech_id-19) + "-card-" + i + "-on-" + j,weight_for_card + temp_weight + getWeightsForMoving(j,tech_id-19));
			    }
			}
		    }

		    //there is probably a better way to do this
		    if (operations == weights.size())
			weights.add("discard-card-" + i,0);

		}
	    }
	}
};
