package brass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

/**
 * Created by mc on 15-Nov-16.
 */
public class BrassOurComputerPlayer implements BrassComputerPlayer {

    private BrassGame brass_game;
    private int player_id;
    public static int depth;

    public BrassOurComputerPlayer(BrassGame bg) {
        this(bg, 0, 3);
    }

    public BrassOurComputerPlayer(BrassGame bg, int depth) {
        this(bg, 0, depth);
    }

    public BrassOurComputerPlayer(BrassGame bg, int player_id, int depth) {
        brass_game = bg;
        this.player_id = player_id;
        this.depth = depth;
    }

    public void setPlayerId(int p_id) {
        player_id = p_id;
    }

    public BrassComputerPlayerAction getBrassMove() {
        assert player_id > 0 : "Set computer player id for BrassOurComputerPlayer before choosing actions.";
        if (brass_game.getPlayerMoney(player_id) < 0) {
            System.out.println("We have no money ):");
        }
        //System.out.println("ASKED TO SELECT AN ACTION");
        /*int[] num_cards = {0,0,0,0};
        for (int i = 0; i < 4; ++i) {
            num_cards[i] = brass_game.getNumCards(i + 1);
        }*/
        BrassComputerPlayerAction best_action = null;
        assert player_id == brass_game.getActivePlayerID() : "active player: " + brass_game.getActivePlayerID() + ", We're not the active player???! (we're " + player_id + ")";
        int best_move_value = Integer.MIN_VALUE;
        List<BrassComputerPlayerAction> all_actions = getMaxActions(brass_game);
        System.out.println("\nTop level generated " + all_actions.size() + " actions.");
        for (BrassComputerPlayerAction action : all_actions) {
            BrassGame child = brass_game.clone();
            /*System.out.println("\nCONSIDERING THIS ACTION:");
            action.printAction(brass_game, player_id);*/
            child.silentComputerPlayerTurn(action);
            int value = minimax(child, Integer.MIN_VALUE, Integer.MAX_VALUE, depth, true);
            if (value > best_move_value || best_action == null) { //pick SOMETHING if anything's possible
                System.out.println("I like this action because it produces a value of " + value + " which is better than what I could get before (" + best_move_value + ")");
                action.printAction(brass_game, player_id);
                best_move_value = value;
                best_action = action;
            }
        }
        /*for (int i = 0; i < 4; ++i) {
            if (num_cards[i] != brass_game.getNumCards(i + 1)) {
                assert false : "Player " + (i + 1) + " had " + num_cards[i] + " cards, but now they have " + brass_game.getNumCards(i + 1);
            }
        }*/
        /*for (BrassCard card : brass_game.getCards(player_id)) {
            if (!card.isVisible()) {
                System.out.println("*//****************************** " + card.getCardName() + " is not visible. Why not?");
            }
        }*/
        System.out.println("Hopefully nothing has changed yet!!");
        /*try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        if (brass_game.getPlayerMoney(player_id) < 0) {
            System.out.println("We have no money ):");
        }
        System.out.println("I CHOSE THIS ACTION:");
        // best_action.printAction(brass_game, player_id);
        return best_action;
    }

    public static int getValue(BrassGame game, int perspective_player_id) {
        int res = 0;
        int rewards[] = new int[4];
        int[][] build_rewards = new int[4][BrassIndustryEnum.values().length];
        for (int player_id = 1; player_id <= 4; ++player_id) {
            rewards[player_id - 1] = 0;
            for (int industry_id = 1; industry_id <= BrassIndustryEnum.values().length; ++industry_id) {
                build_rewards[player_id - 1][industry_id - 1] = 2*game.countPlayersIndustry(industry_id, player_id) +
                        (canBuildIndustry(game, player_id, industry_id) ? 1 : 0);
            }
        }
        for (int industry_id = 1; industry_id <= BrassIndustryEnum.values().length; ++industry_id) {
            int index_of_builder = 0;
            for (int player_id = 1; player_id <= 4; ++player_id) {
                if (build_rewards[player_id - 1][industry_id - 1] > 1) {
                    if (index_of_builder == 0) {
                        // this may be the only player to have built this industry
                        index_of_builder = player_id;
                    }
                    else {
                        // actually, we aren't. So no one is, break out.
                        index_of_builder = 0;
                        break;
                    }
                }
            }
            if (index_of_builder != 0) {
                build_rewards[index_of_builder - 1][industry_id - 1] += 10;
            }
            for (int player_id = 1; player_id <= 4; ++player_id) {
                rewards[player_id - 1] += build_rewards[player_id - 1][industry_id - 1];
            }
        }
        for (int player_id = 1; player_id <= 4; ++player_id) {
            rewards[player_id - 1] += 10*game.getSilentVictoryPoints(player_id);
            int money = game.getPlayerMoney(player_id);
            if (game.getIncome(player_id) < 5) {
                int bad_income = 100*game.getTurnsLeft()*(game.getIncome(player_id) - 5);
                assert bad_income <= 0 : "bad math: turns_left: " + game.getTurnsLeft() + ", player_income: " + game.getIncome(player_id);
                rewards[player_id - 1] += bad_income;
            }
            else {
                rewards[player_id - 1] += game.getTurnsLeft()*game.getIncome(player_id);
            }
            if ((money < 0) || (game.getNumActionsTaken(player_id) == 2 && game.getMoney(player_id) + game.getIncome(player_id) < 0)) {
                // we never want to cause someone to have negative money.
                return (perspective_player_id == player_id) ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            }
            else if (money < 10) {
                rewards[player_id - 1] -= money*10;
            }
            else {
                rewards[player_id - 1] += money;
            }
            if (perspective_player_id == player_id) {
                res += rewards[player_id - 1];
            } else {
                res -= rewards[player_id - 1]/3;
            }
        }
        return res;
    }

    private int minimax(BrassGame game, int alpha, int beta, int depth, boolean maximizing) {
        if (depth == 0 || game.isGameOver()) {
            return getValue(game, player_id);
        }
        assert depth > 0 : "depth: " + depth + " < 0!";
        if (maximizing) {
            int value = Integer.MIN_VALUE;
            for (BrassComputerPlayerAction action : getMaxActions(game)) {
                BrassGame child = game.clone();
                child.silentComputerPlayerTurn(action);
                int value_of_child = minimax(child, alpha, beta, depth - 1, child.getActivePlayerID() == player_id);
                if (value_of_child != Integer.MIN_VALUE) {
                    value = Math.max(value, value_of_child);
                    alpha = Math.max(alpha, value);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return value;
        } else {
            int value = Integer.MAX_VALUE;
            for (BrassComputerPlayerAction action : getMaxActions(game)) {
                BrassGame child = game.clone();
                child.silentComputerPlayerTurn(action);
                int value_of_child = minimax(child, alpha, beta, depth - 1, child.getActivePlayerID() == player_id);
                if (value_of_child != Integer.MAX_VALUE) {
                    value = Math.min(value, value_of_child);
                    beta = Math.min(beta, value);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return value;
        }
        /*
        from Wikipedia
        06          for each child of node
        07              v := max(v, alphabeta(child, depth – 1, α, β, FALSE))
        08              α := max(α, v)
        09              if β ≤ α
        10                  break (* β cut-off *)
        11          return v
        12      else
        13          v := ∞
        14          for each child of node
        15              v := min(v, alphabeta(child, depth – 1, α, β, TRUE))
        16              β := min(β, v)
        17              if β ≤ α
        18                  break (* α cut-off *)
        19          return v
        */
    }

    public List<BrassComputerPlayerAction>  getMaxActions(BrassGame game)
    {
    	List<BrassComputerPlayerAction> actions = new ArrayList<BrassComputerPlayerAction>();
    	int p_id = game.getActivePlayerID();

    	//selling cotton
        if (game.canSellCotton(p_id))
        	getCardsForNonBuildAction(actions, game, BrassActionEnum.SELL);

    	//taking loans, but only as a first move, and limited by player's income and money
        if (game.getNumActionsTaken(p_id) == 0 && game.canTakeLoan(p_id)) {
            int player_money = game.getMoney(p_id);
            int player_income = game.getIncome(p_id);
            if (player_money < 12 && player_income <= 10) {
                // a strategy guide says to always take 30
                getCardsForNonBuildAction(actions, game, BrassActionEnum.LOAN_10, 3);
            }
        }

        //make links
        int num_links = game.getNumLinks();
        for (int i = 1; i <= num_links; i++)
            if (game.canBuildLink(i, p_id))
                getCardsForNonBuildAction(actions, game, BrassActionEnum.LINK, i);
/*

    	//links that immediately allow selling cotton
        for (int i = 1; i <= num_links; i++)
        	if (game.canBuildLink(i, p_id))
        	{
        		game.buildTestLink(i, p_id);
        		if (game.canSellCotton(p_id))
        			getCardsForNonBuildAction(actions, game, BrassActionEnum.LINK, i);
        		game.removeTestLink(i);
        	}
*/

    	//building any of the industries, if the player has none unflipped
    	//upgrading, up to level 2, only while in canal phase

        int num_industries = BrassIndustryEnum.values().length;

        int tech_levels [] = new int [num_industries];
        for (int industry_id = 1; industry_id <= num_industries; industry_id++)
        {
        	//build
            // always try to build, only selects max_to_generate options.
            getAllInfoForIndustrySpecificBuildAction(game, actions, p_id, industry_id);
            //upgrade
            for (int id2 = industry_id; id2 <= num_industries; id2++)
            {
            	if (id2 == industry_id) {
                    tech_levels[id2 - 1] = game.getIndustryTokenLevel(p_id, id2);
                }
            	if (game.canTechUpgrade(p_id) &&
            			!game.getBrassPhase() &&
            			tech_levels[id2-1] <= 1 &&
            			tech_levels[industry_id-1] <= 1 )
            	{
            		if (id2 != industry_id)
            		{
            			if (game.hasIndustryTokens(p_id, industry_id, 1) && game.hasIndustryTokens(p_id, id2, 1))
                    		getCardsForNonBuildAction(actions, game, BrassActionEnum.UPGRADE, industry_id, id2);
            		}
            		else if (game.hasIndustryTokens(p_id, industry_id, 2))
            			getCardsForNonBuildAction(actions, game, BrassActionEnum.UPGRADE, industry_id, id2);
            	}
            }
        }

    	//discarding only if no more than x (5?) options are available
        if (actions.size() == 0) {
            //discards
            getCardsForNonBuildAction(actions, game, BrassActionEnum.DISCARD); // only generates one card...
        }
        assert actions.size() > 0 : "Somehow we failed to generate an action...";
    	return actions;
    }

    //public List getMinActions(BrassGame) ?
    // no.

    public List<BrassComputerPlayerAction> getAllActions(BrassGame game) {
        int p_id = game.getActivePlayerID();
        ArrayList<BrassComputerPlayerAction> actions = new ArrayList<>();

        if (game.isGameOver())
        	return null;

        //build and upgrade industries:
        int num_industries = BrassIndustryEnum.values().length;
        for (int industry_id = 1; industry_id <= num_industries; industry_id++)
        {
        	//build
            getAllInfoForIndustrySpecificBuildAction(game, actions, p_id, industry_id);
            //upgrade
            if (game.canTechUpgrade(p_id)) {
                if (game.hasIndustryTokens(p_id, industry_id, 2))
                    getCardsForNonBuildAction(actions, game, BrassActionEnum.UPGRADE, industry_id, industry_id);
                for (int id2 = industry_id + 1; id2 <= num_industries; id2++) {
                    if (game.canTechUpgrade(p_id)) {
                        if (game.hasIndustryTokens(p_id, industry_id, 1) && game.hasIndustryTokens(p_id, id2, 1))
                            getCardsForNonBuildAction(actions, game, BrassActionEnum.UPGRADE, industry_id, id2);
                    }
                }
            }
        }

        //sell cotton
        if (game.canSellCotton(p_id)) {
        	getCardsForNonBuildAction(actions, game, BrassActionEnum.SELL);
        }

        //make links
        int num_links = game.getNumLinks();
        for (int i = 1; i <= num_links; i++)
        	if (game.canBuildLink(i, p_id))
        		getCardsForNonBuildAction(actions, game, BrassActionEnum.LINK, i);

        //take loans
        for (int i = 1; i <= 3; i++)
        	if (game.canTakeLoan(p_id))
        		getCardsForNonBuildAction(actions, game, BrassActionEnum.LOAN_10, i);

        //discards
        getCardsForNonBuildAction(actions, game, BrassActionEnum.DISCARD);

        //also consider:
        	//double-card build industry
        	//double-rail

        return actions.subList(0, Math.min(actions.size(), 10)); //subList for debug;
    }

    private void getCardsForNonBuildAction(List<BrassComputerPlayerAction> actions, BrassGame game, BrassActionEnum action, int... ids)
    {
    	int player_id = game.getActivePlayerID();
        int old_size = actions.size();
    	//for (int i = 1; i <= num_cards; i++)
    	//{
		int i = getCardForNonBuildAction(game, player_id);
    		//if (!game.canSelectCard(i, player_id)) continue;
        BrassComputerPlayerAction computer_move = new BrassComputerPlayerAction();
        if (action == BrassActionEnum.SELL)
            computer_move.selectSellCottonAction(i);
        else if (action == BrassActionEnum.LINK)
            computer_move.selectLinkAction(i, ids[0]);
        else if (action == BrassActionEnum.LOAN_10) // chooses between all possible loans
            computer_move.selectTakeLoanAction(i, ids[0]);
        else if (action == BrassActionEnum.UPGRADE)
            computer_move.selectTechUpgradeAction(i, ids[0], ids[1]);
        else if (action == BrassActionEnum.DISCARD)
            computer_move.selectDiscardAction(i);
        actions.add(computer_move);
        assert old_size  + 1 == actions.size() : "old_size (" + old_size + ") + 1 != new size(" + actions.size() + ")";
    }

	private int getCardForNonBuildAction(BrassGame game, int player_id)
	{
		int num_cards = game.getNumCards(player_id);
		boolean brass_phase = game.getBrassPhase();

		for (int i = 1; i <= num_cards; i++)
		{
			if (!game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = game.getCardCityTechID(i);

			if (brass_card_city_tech_id < 20)
			{
				int city_id = brass_card_city_tech_id;
				if (game.isCityFull(city_id)) return i;

				//duplicate city cards or already built in city (and canal phase)
				if (!brass_phase)
				{
					int num_tokens_in_city = game.getNumTokensInCity(city_id, player_id);
					if (num_tokens_in_city > 0) return i;

					for (int j = i+1; j <= num_cards; j++)
					{
						int city_id_duplicate = game.getCardCityTechID(j);
						if (city_id == city_id_duplicate) return i;
					}
				}
			}

			if (brass_card_city_tech_id == 24) return i; //shipyard industry card
			if (brass_card_city_tech_id == 1)  return i; //barrow
			if (brass_card_city_tech_id == 2) return i;  //birkenhead

			//duplicate industry cards
			if (brass_card_city_tech_id > 19)
			{
				int industry_id = brass_card_city_tech_id;// - 19;
				for (int j = i+1; j <= num_cards; j++)
				{
					int industry_id_duplicate = game.getCardCityTechID(j);
					if (industry_id == industry_id_duplicate) return i;
				}
			}
		}

		//second pass through the cards
		for (int i = 1; i <= num_cards; i++)
		{
			if (!game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = game.getCardCityTechID(i);

			if (brass_card_city_tech_id == 6) return i;  //bury
			if (brass_card_city_tech_id == 19) return i;  //wigan

			if (brass_card_city_tech_id == 22) return i;  //iron works
			if (brass_card_city_tech_id == 20) return i;  //coal

			if (brass_card_city_tech_id == 14) return i;  //oldham
			if (brass_card_city_tech_id == 5) return i;  //burnley
		}

		 //pick a random card to use for the non build action
		util.Random rand = util.Random.getRandomNumberGenerator();
		int random_discard = rand.randomInt(1, num_cards);
		while (!game.canSelectCard(random_discard, player_id))
		{
			random_discard = rand.randomInt(1, num_cards);
		}
	//System.out.println("random_discard: " + random_discard);
		return random_discard;
	}

	private static boolean canBuildIndustry(BrassGame game, int player_id, int industry_id) {
        int city_id;
        int num_cards = game.getNumCards(player_id);
        for (int i = 1; i <= num_cards; i++) {
            if (!game.canSelectCard(i, player_id)) continue;

            int brass_card_city_tech_id = game.getCardCityTechID(i);
            if (brass_card_city_tech_id <= 19) {
                city_id = brass_card_city_tech_id;
                if (game.canBuildIndustry(true, city_id, industry_id, player_id)) {
                    int coal_city_id = game.canMoveCoal(city_id, industry_id, player_id);
                    int iron_city_id = game.canMoveIron(industry_id, player_id);
                    if (coal_city_id >= 0 && iron_city_id >= 0) {
                        return true;
                    }
                }
            } else {
                int card_industry_id = brass_card_city_tech_id - 19;
                if (industry_id == card_industry_id) {
                    //what cities can this industry be built in?
                    for (int j = 1; j <= 19; j++) {
                        city_id = j;
                        if (game.canBuildIndustry(false, city_id, industry_id, player_id)) {
                            int coal_city_id = game.canMoveCoal(city_id, industry_id, player_id);
                            int iron_city_id = game.canMoveIron(industry_id, player_id);
                            if (coal_city_id >= 0 && iron_city_id >= 0) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private void getAllInfoForIndustrySpecificBuildAction(BrassGame game, List<BrassComputerPlayerAction> computer_moves, int player_id, int industry_id) {
        int max_to_generate = 4;
        //loop over computer player cards
        //find a card that corresponds to something the computer player can build
        //select that card and the build action
        int num_cards = game.getNumCards(player_id);
        int city_id;

        for (int i = 1; i <= num_cards; i++) {
            if (!game.canSelectCard(i, player_id)) continue;

            int brass_card_city_tech_id = game.getCardCityTechID(i);
            if (brass_card_city_tech_id <= 19) {
                city_id = brass_card_city_tech_id;
                if (game.canBuildIndustry(true, city_id, industry_id, player_id)) {
                    int coal_city_id = game.canMoveCoal(city_id, industry_id, player_id);
                    int iron_city_id = game.canMoveIron(industry_id, player_id);
                    if (coal_city_id >= 0 && iron_city_id >= 0) {
                    	BrassComputerPlayerAction computer_move = new BrassComputerPlayerAction();
                        computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
                        computer_moves.add(computer_move);
                        if (--max_to_generate <= 0) {
                            return;
                        }
                    }
                }
            } else {
                int card_industry_id = brass_card_city_tech_id - 19;
                if (industry_id == card_industry_id) {
                    //what cities can this industry be built in?
                    for (int j = 1; j <= 19; j++) {
                        city_id = j;
                        if (game.canBuildIndustry(false, city_id, industry_id, player_id)) {
                            int coal_city_id = game.canMoveCoal(city_id, industry_id, player_id);
                            int iron_city_id = game.canMoveIron(industry_id, player_id);
                            if (coal_city_id >= 0 && iron_city_id >= 0) {
                            	BrassComputerPlayerAction computer_move = new BrassComputerPlayerAction();
                                computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
                                computer_moves.add(computer_move);
                                if (--max_to_generate == 0) {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

/*
    //handles an arbitrary number of connected links
    private void getAllInfoForLinkToSellCottonAction(BrassGame game, BrassComputerPlayerAction computer_move, int computer_player_id, int max_depth_limit) {
        if (max_depth_limit < 1) max_depth_limit = 1;
        int num_connections = game.getNumLinks();

        int curr_depth_limit = 1;
        int curr_depth = 1;

        while (!computer_move.isActionSelected() && curr_depth_limit <= max_depth_limit) {
            //if a good link combination is found, this top level loop
            //determines the link to build
            for (int i = 1; i <= num_connections; i++) {
                //as soon as a link is found, stop
                if (computer_move.isActionSelected()) return;

                boolean can_build_link = game.canBuildLink(i, computer_player_id);
                if (can_build_link) {
                    //temporarily build the link (by simply setting the player_id of the link to computer_player_id)
                    game.buildTestLink(i, computer_player_id);
                    boolean can_player_sell_cotton = game.canSellCotton(computer_player_id);
                    if (can_player_sell_cotton) {
                        computer_move.selectLinkAction(getCardForNonBuildAction(game, computer_player_id), i);
                        System.out.println("Sell cotton link top level");
                    } else if (curr_depth < curr_depth_limit) {
                        getAllInfoForLinkToSellCottonActionRec(game, computer_move, computer_player_id, curr_depth_limit, curr_depth + 1, num_connections, i);
                    }
                    //remove the temporary link (by simply setting the player_id of the link to 0)
                    game.removeTestLink(i);
                }
            }
            curr_depth_limit++;
        }
    }

    private void getAllInfoForLinkToSellCottonActionRec(BrassGame game, BrassComputerPlayerAction computer_move, int computer_player_id, int max_depth, int curr_depth, int num_connections, int first_connection) {
        for (int i = 1; i <= num_connections; i++) {
            //as soon as a link is found, stop
            if (computer_move.isActionSelected()) return;

            boolean can_build_link = game.canBuildLink(i, computer_player_id);
            if (can_build_link) {
                //temporarily build the link (by simply setting the player_id of the link to computer_player_id)
                game.buildTestLink(i, computer_player_id);
                boolean can_player_sell_cotton = game.canSellCotton(computer_player_id);
                if (can_player_sell_cotton) {
                    computer_move.selectLinkAction(getCardForNonBuildAction(game, computer_player_id), first_connection);
                    System.out.println("Sell cotton link depth = " + curr_depth);
                } else if (curr_depth < max_depth) {
                    getAllInfoForLinkToSellCottonActionRec(game, computer_move, computer_player_id, max_depth, curr_depth + 1, num_connections, first_connection);
                }
                //remove the temporary link (by simply setting the player_id of the link to 0)
                game.removeTestLink(i);
            }
        }
    }

    private void getAllInfoForSortedLinkAction(BrassGame game, BrassComputerPlayerAction computer_move, int computer_player_id) {
        List<Integer> sorted_links = game.getSortedLinks();
        int num_connections = sorted_links.size();
        for (int i = 1; i <= num_connections; i++) {
            int link_id = sorted_links.get(i - 1);
            boolean can_build_link = game.canBuildLink(link_id, computer_player_id);
            if (can_build_link) {
                computer_move.selectLinkAction(getCardForNonBuildAction(game, computer_player_id), link_id);
                System.out.println("Sorted links");
                return;
            }
        }
    }

    private void getAllInfoForLinkAction(BrassGame game, BrassComputerPlayerAction computer_move, int computer_player_id) {
        int num_connections = game.getNumLinks();
        for (int i = 1; i <= num_connections; i++) {
            boolean can_build_link = game.canBuildLink(i, computer_player_id);
            if (can_build_link) {
                computer_move.selectLinkAction(getCardForNonBuildAction(game, computer_player_id), i);
                System.out.println("Regular Link");
                return;
            }
        }
    }
*/
}
