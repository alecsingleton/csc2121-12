package brass;

import java.util.List;

class BrassBuyPortsState implements BrassComputerPlayerState
{
	private BrassGame brass_game;
	private BrassAIComputerPlayer computer_player;
	private int num_of_ports;
	
	public BrassBuyPortsState(BrassGame bg, BrassAIComputerPlayer cp)
	{
		brass_game = bg;
		computer_player = cp;
		num_of_ports = 0;
	}
	
	public BrassComputerPlayerAction getBrassMove()
	{
		System.out.println("BUY PORTS STATE");
		boolean brass_phase = brass_game.getBrassPhase();
		int computer_player_id = brass_game.getActivePlayerID();
		int player_money = brass_game.getMoney(computer_player_id);
		int player_income = brass_game.getIncome(computer_player_id);
		if (player_income < 0 && !brass_phase){
			computer_player.changeState(computer_player.getSellCottonState());
			return computer_player.getSellCottonState().getBrassMove();
		}
		else if (player_income < 0 && brass_phase){
			return computer_player.getBuyCoalState().getBrassMove();
		}
		
		BrassComputerPlayerAction computer_move = new BrassComputerPlayerAction();
		
		int num_actions_already_taken = brass_game.getNumActionsTaken(computer_player_id);
		
		//take a loan if necessary (the constants 11 and 10 may need to be changed)

		if (player_money < 11 && player_income <= 10)
		{
			computer_move.selectTakeLoanAction(getCardForNonBuildAction(computer_player_id), 3);
			return computer_move;
		}
		
		//if((brass_phase && num_of_ports < 6) || (!brass_phase && num_of_ports < 4))
		if(num_of_ports < 6)
		{
			getAllInfoForIndustrySpecificBuildAction(computer_move, computer_player_id, BrassIndustryEnum.PORT.getValue());
			
			if (computer_move.isActionSelected()){
				num_of_ports++;
				return computer_move;
			}
		}
		
		if(brass_game.isDeckExhausted() && !brass_phase){
			computer_player.changeState(computer_player.getEndCanalState());
			return computer_player.getEndCanalState().getBrassMove();
		}
		
		if(brass_phase){
			//computer_player.changeState(computer_player.getBuyCoalState());
			return computer_player.getBuyCoalState().getBrassMove();
		}
		
		//sort links by victory points (coins)
		getAllInfoForSortedLinkAction(computer_move, computer_player_id);
		if (computer_move.isActionSelected()) return computer_move;
		
		

		
	System.out.println("Discard!");
		computer_move.selectDiscardAction(getCardForNonBuildAction(computer_player_id));
		return computer_move;
	}

	private int getCardForNonBuildAction(int player_id)
	{
		int num_cards = brass_game.getNumCards(player_id);
		boolean brass_phase = brass_game.getBrassPhase();
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id < 20)
			{
				int city_id = brass_card_city_tech_id;
				if (brass_game.isCityFull(city_id)) return i;
				
				//duplicate city cards or already built in city (and canal phase)
				if (!brass_phase)
				{
					int num_tokens_in_city = brass_game.getNumTokensInCity(city_id, player_id);
					if (num_tokens_in_city > 0) return i;
					
					for (int j = i+1; j <= num_cards; j++)
					{
						int city_id_duplicate = brass_game.getCardCityTechID(j);
						if (city_id == city_id_duplicate) return i;
					}
				}
			}
		
			if (brass_card_city_tech_id == 24) return i; //shipyard industry card
			if (brass_card_city_tech_id == 1)  return i; //barrow
			if (brass_card_city_tech_id == 2) return i;  //birkenhead
			
			//duplicate industry cards
			if (brass_card_city_tech_id > 19 && brass_card_city_tech_id != 23)
			{
				int industry_id = brass_card_city_tech_id;// - 19;
				for (int j = i+1; j <= num_cards; j++)
				{
					int industry_id_duplicate = brass_game.getCardCityTechID(j);
					if (industry_id == industry_id_duplicate) return i;
				}
			}
		}
		
		//second pass through the cards
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id == 6) return i;  //bury
			if (brass_card_city_tech_id == 19) return i;  //wigan
			
			if (brass_card_city_tech_id == 22) return i;  //iron works
			if (brass_card_city_tech_id == 20) return i;  //coal
			
			if (brass_card_city_tech_id == 14) return i;  //oldham
			if (brass_card_city_tech_id == 5) return i;  //burnley
		}
		 
		 //pick a random card to use for the non build action
		util.Random rand = util.Random.getRandomNumberGenerator();
		int random_discard = rand.randomInt(1, num_cards);
		while (!brass_game.canSelectCard(random_discard, player_id))
		{
			random_discard = rand.randomInt(1, num_cards);
		}
	//System.out.println("random_discard: " + random_discard);
		return random_discard;
	}
	
	private void getAllInfoForIndustrySpecificBuildAction(BrassComputerPlayerAction computer_move, int player_id, int industry_id)
	{
		//loop over computer player cards
		//find a card that corresponds to something the computer player can build
		//select that card and the build action
		int num_cards = brass_game.getNumCards(player_id);
		int city_id;
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
				
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			if (brass_card_city_tech_id > 19)
			{
				int card_industry_id = brass_card_city_tech_id - 19;
				if (industry_id == card_industry_id)
				{
					//what cities can this industry be built in?
					for (int j = 1; j <= 19; j++)
					{
						city_id = j;
						if (brass_game.canBuildIndustry(false, city_id, industry_id, player_id))
						{
							int coal_city_id = brass_game.canMoveCoal(city_id, industry_id, player_id);
							int iron_city_id = brass_game.canMoveIron(industry_id, player_id);
							if (coal_city_id >= 0 && iron_city_id >= 0) 
							{
								computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
								return;
							}
						}
					}
				}
			}
			else
			{
				city_id = brass_card_city_tech_id;
				if (brass_game.canBuildIndustry(true, city_id, industry_id, player_id))
				{
					int coal_city_id = brass_game.canMoveCoal(city_id, industry_id, player_id);
					int iron_city_id = brass_game.canMoveIron(industry_id, player_id);
					if (coal_city_id >= 0 && iron_city_id >= 0) 
					{
						computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
						return;
					}
				}
			}
		}
	}
	
	private void getAllInfoForSortedLinkAction(BrassComputerPlayerAction computer_move, int computer_player_id)
	{	
		List<Integer> sorted_links = brass_game.getSortedLinks();
		int num_connections = sorted_links.size();
		for (int i = 1; i <= num_connections; i++)
		{
			int link_id = sorted_links.get(i-1);
			boolean can_build_link = brass_game.canBuildLink(link_id, computer_player_id);
			if (can_build_link)
			{
				computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), link_id);
System.out.println("Sorted links");				
				return;
			}
		}
	}
	
	
}
